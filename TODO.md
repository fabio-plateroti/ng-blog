# NG Blog
Progetto per lo studio di Angular 2+.
Il progetto prevederà le seguenti sezioni:
* Home page
* Sezione "Blog" con i post più recenti
* Sezione "Albums"
* Sezione "Admin User" privata al solo profilo di "Super User"


## Task
- [WIP] Inizializzare il progetto
  - [OK] Creare progetto angular
  - [OK] Aggiungere Bootstrap 5
  - [WIP] Preparare template https://getbootstrap.com/docs/5.1/examples/blog/
  - [OK] Aggiungere NG-Bootstrap
  - [OK] Avviare il server con template e ng-bootstrap inclusi
- [...] Sezione BLOG
- [WIP] Sezione ALBUMBS
    - [OK] Lista di album con eventuale preview
    - [...] CRUD album
    - [...] dettaglio album: elenco foto
    - [...] CRUD foto
- [...] Sezione LOGIN
- [...] Sezione ADMIN
- [WIP] Homepage