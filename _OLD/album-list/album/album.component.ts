import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit {

    @Input() prova2:any;

    
    private id: number;
    private nome:string;
    private data_creazione:Date;
    private dimensione:number;
    private numero_foto:number;

    public setId(id:number):void{
        this.id=id;
    }
    public getId():number{
        return this.id;
    }
    public setNome(nome:string):void{
        this.nome=nome;
    }
    public getNome():string{
        return this.nome;
    }
    public setDataCreazione(data_creazione:Date):void{
        this.data_creazione=data_creazione;
    }

    public getDataCreazione():Date{
        return this.data_creazione;
    }

    public setDimensione(dimensione:number):void{
        this.dimensione=dimensione;
    }
  
    public getDimensione():number{
        return this.dimensione;
    }

    public getNumeroFoto(numero_foto:number):void{
        this.numero_foto=numero_foto;
    }

    public setNumeroFoto():number{
        return this.numero_foto;
    }

  constructor() { 
    

  }

  ngOnInit() {
  }

}
