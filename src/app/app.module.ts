import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { LayoutFooterComponent } from './layout/footer/footer.component';
import { LayoutHeaderComponent } from './layout/header/header.component';
import { PagesAlbumCreateComponent } from './pages/album-create/album-create.component';
import { PagesAlbumDetailComponent } from './pages/album-detail/album-detail.component';
import { PagesAlbumListComponent } from './pages/album-list/album-list.component';
import { PagesError404Component } from './pages/error-404/error-404.component';
import { PagesHomepageComponent } from './pages/homepage/homepage.component';


@NgModule({
    imports: [
        BrowserModule,
        AppRoutingModule,
    ],
    declarations: [
        AppComponent,
        LayoutHeaderComponent,
        LayoutFooterComponent,
        PagesAlbumDetailComponent,
        PagesAlbumListComponent,
        PagesError404Component,
        PagesHomepageComponent,
        PagesAlbumCreateComponent
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
