import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesAlbumCreateComponent } from './pages/album-create/album-create.component';
import { PagesAlbumDetailComponent } from './pages/album-detail/album-detail.component';
import { PagesAlbumListComponent } from './pages/album-list/album-list.component';
import { PagesError404Component } from './pages/error-404/error-404.component';
import { PagesHomepageComponent } from './pages/homepage/homepage.component';

const routes: Routes = [
    { path: 'album/:id', component: PagesAlbumDetailComponent },
    { path: 'album', component: PagesAlbumListComponent },
    { path: 'home', component: PagesHomepageComponent },
    { path: 'create', component: PagesAlbumCreateComponent },

    // { path: 'blog', component: BlogComponent },

    { path: '', redirectTo: '/home', pathMatch: 'full' }, // redirect to `first-component`
    { path: '**', component: PagesError404Component, },  // Wildcard route for a 404 page
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        useHash: true,
        onSameUrlNavigation: 'reload',
        scrollPositionRestoration: 'top',
    })],
    exports: [RouterModule]
})

export class AppRoutingModule { }
