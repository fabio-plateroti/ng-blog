import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlbumService } from 'src/app/services/album.service';

@Component({
    selector: 'app-pages-album-detail',
    templateUrl: './album-detail.component.html',
})
export class PagesAlbumDetailComponent {

    public albumDetail: any;

    constructor(
        private albumService: AlbumService,
        private activatedRoute: ActivatedRoute,
    ) {
        this.albumDetail = this.albumService.getById(+this.activatedRoute.snapshot.params.id);
    }

}
