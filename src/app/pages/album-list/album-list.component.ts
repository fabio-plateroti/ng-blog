import { Component } from '@angular/core';
import { AlbumService } from 'src/app/services/album.service';

@Component({
    selector: 'app-pages-album-list',
    templateUrl: './album-list.component.html',
})
export class PagesAlbumListComponent {

    public albums: any[] = [];

    constructor(private albumService: AlbumService,) {
        this.albums = this.albumService.getAll();
    
        
    }

    deleteAlbum(albums:any){
        this.albumService.deleteAlbum(albums);
        
    }

   
}

